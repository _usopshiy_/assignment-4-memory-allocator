#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>


#define get_block_header(memory) ((struct block_header*) (((uint8_t*) (memory)) - offsetof(struct block_header, contents)))

#define MAP_ANONYMOUS 0x20


static char* malloc_test(void){
    void* heap = heap_init(0);
    assert(heap);

    void* allocated = _malloc(4096);

    assert(allocated);

    heap_term();

    return "Malloc test passed succesfully";
}

static char* free_one_test(void){
    struct region* heap = heap_init(0);
    assert(heap);

    void* block1 = _malloc(0);
    assert(block1);
    
    void* block2 = _malloc(100);
    assert(block2);

    void* block3 = _malloc(200);
    assert(block3);

    _free(block3);

    assert(!(get_block_header(block1)->is_free));
    assert(!(get_block_header(block2)->is_free));
    assert(get_block_header(block3)->is_free);

    heap_term();

    return "Free one of few blocks test passed succesfully";
}

static char* free_two_test(void){
    struct region* heap = heap_init(0);
    assert(heap);

    void* block1 = _malloc(0);
    assert(block1);
    
    void* block2 = _malloc(100);
    assert(block2);

    void* block3 = _malloc(200);
    assert(block3);

    _free(block1);

    assert(get_block_header(block1)->is_free);
    assert(get_block_header(block2)->is_free);
    assert(!(get_block_header(block3)->is_free));


    _free(block3);

    assert(!(get_block_header(block1)->is_free));
    assert(get_block_header(block2)->is_free);
    assert(!(get_block_header(block3)->is_free));

    heap_term();

    return "Free two of few blocks test passed succesfully";
}

static char* extend_region_test(void){
    struct region* heap = heap_init(0);
    assert(heap);

    size_t heap_initial = heap->size;
    _malloc(4096);
    size_t heap_after = heap->size;

    assert(heap_initial < heap_after);

    heap_term();

    return "Extend region test passed succefully";
}

static char* extend_filled_region_test(void){

    void* initial_alloc_ptr = mmap(HEAP_START, 10, 0, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(initial_alloc_ptr);

    void* additional_alloc_ptr = _malloc(10);
    assert(additional_alloc_ptr);
    
    assert(initial_alloc_ptr != additional_alloc_ptr);

    heap_term();

    return "Extend filled region test passed succefully";
}

static void do_test(char* (test)(void), uint8_t test_number){
    printf("Running test #%u\n", test_number);
    char* mes = test();
    printf("#%u: %s\n", test_number, mes);
}


int main(){
    do_test(malloc_test, 1);
    do_test(free_one_test, 2);
    do_test(free_two_test, 3);
    do_test(extend_region_test, 4);
    do_test(extend_filled_region_test, 5);


    return 0;
}